# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

if [ -d "$HOME/.brew/bin" ] ; then
    PATH="$HOME/.brew/bin:$PATH"
fi

# Rust binaries
export PATH="$HOME/.cargo/bin:$PATH"
# Ruby binaries
export PATH="$HOME/.rbenv/bin:$PATH"
#eval "$(rbenv init -)"
