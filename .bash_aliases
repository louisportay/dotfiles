alias project="cd ~/Documents/computorv1"
alias eman="man -L C"
alias rgrep="grep -rHn --color=auto"
alias batstat="upower -i /org/freedesktop/UPower/devices/battery_BAT0"
alias val-fl="valgrind --leak-check=full"
alias e="emacs -nw"
alias dirsize="du -h --max-depth=0"
alias ls-packages-by-size="dpkg-query -Wf '${Installed-Size}\t${Package}\n' | sort -n"
alias ll='ls -l'
alias dotfiles='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
