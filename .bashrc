# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

#if [[ -z $TMUX ]]; then
#	tmux new-session -s "work"
#fi

HISTCONTROL=ignoreboth
HISTSIZE=2000
HISTFILESIZE=2000
PS1="[\W]$ "

export LANG="fr_FR.UTF-8"
export MANPAGER="/usr/bin/most"
export PAGER="/usr/bin/most"
export EDITOR="/usr/bin/vim"
shopt -s checkwinsize
shopt -s histappend
shopt -s autocd
shopt -s xpg_echo
shopt -s globstar
shopt -s extglob
