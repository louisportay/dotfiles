set nocompatible
set fileencoding=utf-8
set term=xterm-256color

syntax on
set cindent
set number
colorscheme desert
set hlsearch
map <TAB><TAB>	:noh<CR>

"deactivate keys to navigate
nnoremap <Up> <NOP>
nnoremap <Down> <NOP>
nnoremap <Left> <NOP>
nnoremap <Right> <NOP>
nnoremap <Up> <NOP>
inoremap <Down> <NOP>
inoremap <Left> <NOP>
inoremap <Right> <NOP>
inoremap <ESC> <NOP>

inoremap jk <ESC>
set shiftwidth=4
set tabstop=4
set noexpandtab
set softtabstop=0
set mouse=a
set clipboard=unnamedplus
set autoread
set backspace=indent,eol,start
set ruler

" Rust
autocmd BufRead *.rs setlocal tags=./rusty-tags.vi;/

" Python
filetype indent plugin on

" Ruby
autocmd Filetype ruby set ts=2
autocmd Filetype ruby set sw=2
autocmd Filetype ruby set softtabstop=2
autocmd FileType ruby set expandtab

augroup templates
	" Ruby
	"autocmd BufNewFile *.rb 0r ~/.vim/templates/skeleton.rb
	" HTML
	autocmd BufNewFile *.html 0r ~/.vim/templates/skeleton.html
	" C
	"autocmd BufNewFile *.c 0r ~/.vim/templates/skeleton.c
	" ASM
	autocmd BufNewFile *.s 0r ~/.vim/templates/skeleton.s
	" CPP class
	autocmd BufNewFile *.hpp call NewCppClass()
	" Ocaml
	autocmd BufNewFile *.ml 0r ~/.vim/templates/skeleton.ml

augroup END

" Executable Scripts
"autocmd BufNewFile *.rb  :write | !chmod +x %
"autocmd BufNewFile *.py  :write | !chmod +x %
"autocmd BufNewFile *.sh  :write | !chmod +x %
